---
title: Remote Work
---

Collecting notes and links about working remotely.

Recruiters and Job Listings
===========================
* [Remote Working](http://www.remoteworking.co/), remote job listing
* [Job Spring Partners](http://www.jobspringpartners.com/), Toronto recruiters
  who handle remote positions
* [Syndesus](http://syndesus.com/), recruiter for Canadians to work with
  American companies
* [WFH.io](https://www.wfh.io/)
* [FOSS Jobs - Worldwide](https://www.fossjobs.net/jobs-worldwide/)

Other lists of resources
========================
* http://blog.remoteworknewsletter.com/2015/03/23/best-sources-to-find-a-remote-job-as-a-software-developer/
