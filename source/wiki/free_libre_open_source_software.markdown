---
title: Free Libre Open Source Software
---

* [Waterloo Free Software](/wiki/waterloo_free_software_portal)
* [Waterloo FLOSS Club](/wiki/waterloo_floss_club)

# Events in Canada

* [Pycon](https://pycon.ca/)

# Open Source Business Models and Sustainability

Noting things that have been said about sustaining FLOSS projects.

* https://medium.com/@nayafia/how-i-stumbled-upon-the-internet-s-biggest-blind-spot-b9aa23618c58
* https://medium.com/@nayafia/open-source-infrastructure-the-q-a-a44615861944
* https://medium.com/@nayafia/open-source-was-worth-at-least-143m-of-instagram-s-1b-acquisition-808bb85e4681#.w58lpwmps
* https://medium.com/@nayafia/we-re-in-a-brave-new-post-open-source-world-56ef46d152a3#.1yyreuae7
* https://opensource.com/article/18/1/how-start-open-source-program-your-company

# Sources of funding

* [Code for Canada Fellowships](http://www.codefor.ca/apply)
* [Google Summer of Code](https://summerofcode.withgoogle.com/)
* [Mozilla Open Source Support(MOSS)](https://www.mozilla.org/en-US/moss/)
* [Outreachy](https://www.outreachy.org/)

# Getting started

There are various projects which are working to make it easier for new
contributors. The following contains projects which mark issues beginner
friendly, list of those projects, or tools to help find those projects.

* [Awesome First PR Opportunities](https://github.com/MunGell/awesome-for-beginners)
* [OpenHatch](https://openhatch.org/)
* [Up for grabs](http://up-for-grabs.net/#/)
* [Your First PR](https://yourfirstpr.github.io/)

## Advice and discussions

* [6 Staring Points for Open Source Beginners](https://opensource.com/life/16/1/6-beginner-open-source)
* [Request For Commits – Episode #10: Finding New Contributors](https://changelog.com/rfc/10)

# Other tools for finding issues to work on

There are tools for finding things to work on in FLOSS projects, but are not
specifically for beginners.

* [BugsAhoy](http://www.joshmatthews.net/bugsahoy/), which search open issues
  in [Mozilla](https://www.mozilla.org/) projects
* [Code Triage](https://www.codetriage.com/)
