---
title: Waterloo Region Telecom
---

Research and notes about the consumer telecom options available in [Waterloo
Region](http://wikipedia.org/wiki/Waterloo_Region).

* location of the [Bell Central offices](https://maps.google.ca/maps/ms?msa=0&msid=200956085987439049963.0004889d10c1cf5abf9ba&ie=UTF8&ll=43.443697,-80.482149&spn=0.052907,0.111494&t=m&z=14&vpsrc=6)
* [Wikipedia Internet in Canada](https://en.wikipedia.org/wiki/Internet_in_Canada)

Cheapest Packages
=================

|Company    |Type   |Down/Up   |Cap  |Price   |
|-----------|-------|----------|-----|--------|
|VMedia     |FTTN   |10/1      |-    |$32.95  |
|Teksavvy   |FTTN   |15/1      |300  |$32.99  |
|Teksavvy   |CABLE  |10/1      |75   |$34.95  |
|Teksavvy   |FTTN   |15/10     |300  |$34.99  |
|Teksavvy   |FTTN   |25/10     |300  |$39.99  |
|IGS        |FTTN   |10/7      |-    |$41.95  |
|Teksavvy   |CABLE  |30/5      |150  |$44.99  |
|VMedia     |FTTN   |50/10     |-    |$49.94  |
|Teksavvy   |CABLE  |60/10     |150  |$59.99  |
|Rogers     |CABLE  |150/15    |350  |$85.99  |
|BELL       |FTTN   |175/175   |300  |$89.95  |
|Rogers     |CABLE  |250/20    |500  |$99.99  |
|Rogers     |CABLE  |350/350   |2000 |$225.99 |

Comparisons
===========

Slow DSL/FTTN
-------------
|Company    |Type   |Down/Up   |Cap  |Price   |
|-----------|-------|----------|-----|--------|
|Teksavvy   |DSL    |6/.8      |75   |$26.99  |
|BELL       |FTTN   |5/?       |40   |$47.95  |
|Teksavvy   |FTTN   |7/1       |75   |$24.99  |
|Eyesurf    |FTTN   |7/?       |-    |$39.95  |
|IGS        |FTTN   |7/1       |-    |$37.95  |

FTTN 10/1
---------
|Company    |Type   |Down/Up   |Cap  |Price   |
|-----------|-------|----------|-----|--------|
|VMedia     |FTTN   |10/1      |-    |$32.95  |
|Teksavvy   |FTTN   |10/1      |300  |$32.99  |
|Worldline  |FTTN   |10/1      |-    |$34.95  |
|Acanac     |FTTN   |10/1      |-    |$34.95  |
|IGS        |FTTN   |10/1      |-    |$39.95  |
|Distributel|FTTN   |10/1      |-    |$42.95  |

FTTN 10/7
---------
|Company    |Type   |Down/Up   |Cap  |Price   |
|-----------|-------|----------|-----|--------|
|IGS        |FTTN   |10/7      |-    |$41.95  |
|Distributel|FTTN   |10/7      |-    |$44.95  |
|Eyesurf    |FTTN   |10/?      |-    |$44.95  |

FTTN 15/1
---------
|Company    |Type   |Down/Up   |Cap  |Price   |
|-----------|-------|----------|-----|--------|
|Teksavvy   |FTTN   |15/1      |300  |$32.99  |
|VMedia     |FTTN   |15/1      |-    |$34.95  |
|Acanac     |FTTN   |15/1      |-    |$39.95  |
|Worldline  |FTTN   |15/1      |-    |$39.95  |
|Start      |FTTN   |15/1      |300  |$40     |
|IGS        |FTTN   |15/1      |-    |$42.95  |
|Eyesurf    |FTTN   |15/?      |-    |$49.95  |
|BELL       |FTTN   |15/?      |80   |$57.95  |

FTTN 15/10
----------
|Company    |Type   |Down/Up   |Cap  |Price   |
|-----------|-------|----------|-----|--------|
|Teksavvy   |FTTN   |15/10     |300  |$34.99  |
|Yak        |FTTN   |15/10     |150  |$37.95  |
|IGS        |FTTN   |15/10     |-    |$44.95  |

FTTN 25/10
----------
|Company    |Type   |Down/Up   |Cap  |Price   |
|-----------|-------|----------|-----|--------|
|Teksavvy   |FTTN   |25/10     |300  |$39.99  |
|VMedia     |FTTN   |25/10     |-    |$42.95  |
|Acanac     |FTTN   |25/10     |-    |$44.95  |
|Yak        |FTTN   |25/10     |200  |$44.95  |
|IGS        |FTTN   |25/10     |-    |$48.95  |
|Distributel|FTTN   |25/10     |-    |$49.95  |
|Worldline  |FTTN   |25/10     |-    |$49.95  |
|Start      |FTTN   |25/10     |300  |$50     |
|Eyesurf    |FTTN   |25/?      |-    |$59.95  |
|BELL       |FTTN   |25/10     |100  |$61.95  |

FTTN 50/10
----------
|Company    |Type   |Down/Up   |Cap  |Price   |
|-----------|-------|----------|-----|--------|
|VMedia     |FTTN   |50/10     |-    |$49.94  |
|Teksavvy   |FTTN   |50/10     |300  |$54.99  |
|Yak        |FTTN   |50/10     |300  |$55.95  |
|Worldline  |FTTN   |50/10     |-    |$59.95  |
|Start      |FTTN   |50/10     |300  |$60     |
|BELL       |FTTN   |50/?      |150  |$69.95  |
|Acanac     |FTTN   |50/10     |-    |$69.95  |
|Eyesurf    |FTTN   |50/?      |-    |$69.95  |
|IGS        |FTTN   |50/10     |-    |$69.95  |

FTTN Other
----------
|Company    |Type   |Down/Up   |Cap  |Price   |
|-----------|-------|----------|-----|--------|
|BELL       |FTTN   |175/175   |300  |$89.95  |

CABLE 10/1
----------
|Company    |Type   |Down/Up   |Cap  |Price   |
|-----------|-------|----------|-----|--------|
|Teksavvy   |CABLE  |10/1      |75   |$34.95  |
|Start      |CABLE  |10/1      |100  |$35     |
|Distributel|CABLE  |10/1      |100  |$36.95  |
|IGS        |CABLE  |10/1      |-    |$46.95  |
|Yak        |CABLE  |10/1      |-    |$47.95  |
|Rogers     |CABLE  |10/1      |25   |$51.99  |

CABLE 30/5
----------
|Company    |Type   |Down/Up   |Cap  |Price   |
|-----------|-------|----------|-----|--------|
|Teksavvy   |CABLE  |30/5      |150  |$44.99  |
|Start      |CABLE  |30/5      |200  |$45     |
|VMedia     |CABLE  |30/5      |-    |$49.95  |
|Vintone    |CABLE  |30/5      |-    |$49.99  |
|Acanac     |CABLE  |30/5      |350  |$54.95  |
|Distributel|CABLE  |30/5      |200  |$54.95  |
|Worldline  |CABLE  |30/5      |-    |$54.95  |
|Rogers     |CABLE  |30/5      |70   |$61.99  |
|Yak        |CABLE  |30/5      |-    |$64.95  |
|IGS        |CABLE  |30/10     |-    |$64.95  |

CABLE 60/10
----------
|Company    |Type   |Down/Up   |Cap  |Price   |
|-----------|-------|----------|-----|--------|
|Teksavvy   |CABLE  |60/10     |150  |$59.99  |
|VMedia     |CABLE  |60/10     |-    |$64.95  |
|Vintone    |CABLE  |60/10     |-    |$64.99  |
|Start      |CABLE  |60/10     |300  |$65     |
|Worldline  |CABLE  |60/10     |-    |$69.95  |
|Acanac     |CABLE  |60/10     |350  |$69.95  |
|Rogers     |CABLE  |60/10     |120  |$69.99  |
|Yak        |CABLE  |60/10     |-    |$79.96  |
|IGS        |CABLE  |60/10     |-    |$79.95  |

CABLE other
-----------
|Company    |Type   |Down/Up   |Cap  |Price   |
|-----------|-------|----------|-----|--------|
|Rogers     |CABLE  |150/15    |350  |$85.99  |
|Rogers     |CABLE  |250/20    |500  |$99.99  |
|Rogers     |CABLE  |350/350   |2000 |$225.99 |

skipped Execulink

start is +$10 for unlimited service DSL +$15 for unlimited cable


Sources:
--------

* http://www.bell.ca/Bell_Internet/Internet_access - 2014-11-21
* http://teksavvy.com/en/residential/internet/dsl - 2014-11-21

ISPs
==========================
* [Acanac](http://www.acanac.ca/)
* [Bell](http://www.bell.ca)
* [Distributel](https://www.distributel.ca)
* [Execulink](http://www.execulink.ca/)
* [Eyesurf](http://eyesurf.net)
* [IGS](http://www.kw.igs.net)
* [Rogers](http://www.rogers.ca)
* [Start](https://www.start.ca)
* [Teksavvy](http://www.teksavvy.ca)
* [VMedia](http://vmedia.ca)
* [Worldline](http://www.worldline.ca/services/digital-home-phone/)
* [Yak](http://yak.ca)
* [Vintone](http://vintone.ca/)


VoIP
====
* [Unlimitel](http://www.unlimitel.ca/)
* [voip.ms](http://voip.ms/)
* [NetTalk](http://nettalk.ca)
* [Talkit](http://talkit.ca/)
* [Ooma](http://ca.ooma.com/)
* [MagicJack](https://www.magicjack.com/)
* [Yak](http://yak.ca)
* [TekTalk(Teksavvy)](http://teksavvy.com/en/residential/phone/tektalk)
* [Eyesurf](http://www.eyesurf.ca/phone.html)

References
==========
* [KWLUG August 2014 discussion](http://kwlug.org/pipermail/kwlug-disc_kwlug.org/2014-August/013162.html)
* [KWLUG October 2014 VoIP discussion](http://kwlug.org/pipermail/kwlug-disc_kwlug.org/2014-October/013523.html)
