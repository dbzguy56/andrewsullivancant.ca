---
title: Cream of Wheat
---

Ingredients
===========

* 1 tsp butter
* 1 cup cream of wheat
* 4 cups water
* 1 cup milk
* 1/2 tsp cinnamon
* 1/4 tsp salt
* 2 tbsp honey
* 1/2 tsp vanilla

Instructions
============
* melt the butter in a saucepan on medium heat
* add the cream of wheat and whisk constantly, until browned
* add water and milk whisking constantly
* turn down to low heat
* add cinnamon, salt and honey (and make sure you keep stirring)
* cook for about 10 minutes (stirring constantly), or until the mixture is thickened

Original Recipe from [Chowhound](http://chowhound.chow.com/topics/296154)
