---
:title: 'Notes for KWRuby April 2016: Heroku and Ansible'
:tags: kwruby deployment heroku ansible
:date: 2016-07-10
---
[Eric Roberts](http://www.ericroberts.ca/) and [Jesse McGinnis](https://jcmcginnis.com/) presented 2 different options for deploying a Rails project at the [KWRuby meeting on April 19th 2016](http://www.meetup.com/kw-ruby-on-rails/events/230126200/).

# Eric Roberts: Heroku
* Eric's advice is to just use [Heroku](https://www.heroku.com/) when starting a new project
* it is cheap, easy and allows you to ignore the deployment details on new project
* Heroku deploys with a git push
* recently added support for doing deployments from PR into staging environments
* the free tier now limits server to running 13 hours/day
  - this eliminates a previous hack of keeping a server running by pinging it regularly
  - still more than enough for most purposes

# Jesse McGinnis: Deploy Rails with Ansible
* Jesse has recently been using [Ansible](https://www.ansible.com/) for doing server setup and deployments
* integration with [Vagrant](https://www.vagrantup.com/) for local development, but the configuration can get quirky as it gets complicated
* [Boltmade](http://www.boltmade.com/) has been using it for VM setup and deploys
* Ansible also includes an [inventory system](https://docs.ansible.com/ansible/intro_inventory.html)
  - handle dynamic  and environment based configurations
  - Jesse has not needed to use it, so far
* [Ansible galaxy roles](https://galaxy.ansible.com/) <=> [Ruby gems](https://rubygems.org/)
* Jessie finds that there is still a temptation to start a new system with [bash](https://en.wikipedia.org/wiki/Bash_(Unix_shell))
  - the assumption is that **THIS** system will not get more complicated, but
    they nearly always do
  - his lesson was to just always use Ansible
  - its not much more complicated than bash to start with, and avoid needing
    a re-write once it gets too complicated for bash
