---
:title: 'Notes for KWRuby February 2016: Technical Debt'
:tags: kwruby technical_debt
:date: 2016-04-09
---

[Declan Whelan](http://dpwhelan.com/) presented to [KWRuby](http://kwruby.ca) meeting on [February 17](http://www.meetup.com/kw-ruby-on-rails/events/228077768/). He spoke about how to deal with [technical debt](https://en.wikipedia.org/wiki/Technical_debt) in [agile software](https://en.wikipedia.org/wiki/Agile_software_development) projects:

* Declan is a director of [Agile Alliance](https://agilealliance.org/), who recently met in [Costa Rica](https://en.wikipedia.org/wiki/Costa_Rica)
* he is also co-chairing an Agile Alliance [tech debt working group](https://www.agilealliance.org/resources/initiatives/technical-debt/), which is a focus for him in 2016

Definition
==========
* old code bases tend to have lots of debt which slows down development
* Martin Fowler described the [tech debt quadrants](http://martinfowler.com/bliki/TechnicalDebtQuadrant.html) (i.e., deliberate vs inadvertent, reckless vs prudent)
* the term was originally coined by [Ward Cunningham](https://en.wikipedia.org/wiki/Ward_Cunningham) when describing technical trade-offs to the financial people on a project
* Declan's definition is: **anything in the code that slows me down**
* code which does not map well to the problem domain
* badly written code, however, should not be considered technical debt (e.g., not tests, not TDD, not [SOLID](https://en.wikipedia.org/wiki/SOLID_(object-oriented_design), not meeting [XP simple design principles](http://www.c2.com/cgi/wiki?XpSimplicityRules))
* Simon pointed out that technical debt can slow down paying feature debt, and
  that it can change as features change over time
* [Domain Driven Design](https://en.wikipedia.org/wiki/Domain-driven_design)(DDD) can help my continuously driving the code back towards the problem domain

Measuring
=========
* there are various software metrics which can be used measure/estimate technical debt (e.g., various kinds of [static analysis](https://en.wikipedia.org/wiki/Static_program_analysis), churn analysis)
* the Agile Alliance working from is trying to develop tools for analyzing
  finance and risk related to technical debt in a project
* [Garter](https://www.gartner.com) wrote a technical debt [report in 2010](https://www.gartner.com/newsroom/id/1439513)
  - estimated the global cost of technical debt was $500 billion, and
    expected it to rise to $1 trillion by 2015
* a more informally definition could include
  - how happy are you in the code
  - what % of time is wasted
  - what would it take to fix

Dealing with it
===============
* increasing tech debt will increase the desire for a re-write and it is well establish that a re-write can be VERY risky
  - this is something that Declan has seen in large companies
* it is a deeper problem than "our developers write crappy code"
* What are the underlying reasons?
  - [system archetypes](https://en.wikipedia.org/wiki/System_archetype)
  - [shifting the burden](https://en.wikipedia.org/wiki/System_archetype#Shifting_the_burden)
* over time is gets hard to do fundamental fixes
  - this gets worse unless there is specific push back on this
* our [cognitive bias](https://en.wikipedia.org/wiki/Cognitive_bias) of [recency bias](https://en.wikipedia.org/wiki/Serial_position_effect#Recency_effect), leads us to short term/fast solutions
  - one strategy is to not offer short term/faster options to client, because they will always chooser faster
* forward looking leaders are important to choosing slower but longer term
  solutions
* legal liability may eventually apply some pressure
  - [Uncle Bob](https://twitter.com/unclebobmartin) had discussed this before
  - Declan did an interviewed with [Martin Fowler] and Uncle Bob at Remote Agile Conference
  - maybe some big catastrophic event will precipitate this
* avoid short term project which build on underlying products
  - this encourages the short term choices
  - instead align team/projects to maintaining the products over the long term
* [Test Driven Developer](https://en.wikipedia.org/wiki/Test-driven_development)(TDD) is a useful tool for guiding your code towards the better long term choices
* [Scrum](https://en.wikipedia.org/wiki/Scrum_(software_development) has been a problem because is frequently adopted as a management practise without also adopting a corresponding technical process (i.e., Agile engineering practices, XP) such as:
  - [Test Driven Developer](https://en.wikipedia.org/wiki/Test-driven_development)(TDD)
  - [Pair programming](https://en.wikipedia.org/wiki/Pair_programming)
  - [refactoring](https://en.wikipedia.org/wiki/Code_refactoring)
  - [simple design principles](http://www.c2.com/cgi/wiki?XpSimplicityRules)
    * passes all the tests.
    * expresses every idea that we need to express
    * says everything once and only once (or [Don't Repeat Yourself](https://en.wikipedia.org/wiki/Don't_repeat_yourself){DRY})
    * has no superfluous parts.
  - [collective ownership](https://en.wikipedia.org/wiki/Extreme_programming_practices#Collective_code_ownership)
  - [coding standards](https://en.wikipedia.org/wiki/Extreme_programming_practices#Coding_standard)
  - automated customer tests (or [acceptance tests](http://www.extremeprogramming.org/rules/functionaltests.html)
  - [Continuous Integration](https://en.wikipedia.org/wiki/Continuous_integration)

* some suggestions from the audience for helping with technical debt
  - business agreement
  - static analysis
  - explicitly organized dealing with technical debt with epics, sprints, or chore stories
  - education
