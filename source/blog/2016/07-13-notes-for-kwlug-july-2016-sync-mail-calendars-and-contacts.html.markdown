---
:title: 'Notes for KWLUG July 2016: Sync mail, calendars and contacts'
:tags: kwlug synchronization mail calendar contact pim
:date: 2016-07-13
---
[Chris Irwin](https://chrisirwin.ca) described his recent experiments with mail/calendar/contact
synchronization to [KWLUG](http://kwlug.org) on [July 4 2016](http://kwlug.org/node/1028).
He discussed his goals, the options he considered, and what he is using currently.

* Chris is only creating a system for a single user or small family of users
  - multi-user support is not necessary
* [baikal](http://sabre.io/baikal/) is the calendar server with a very simple
  user interface
* Chris has mostly moved back to google
  - on android, even if sync is disabled, it still puts things into the primary google calendar and not the self-hosted one
    * this makes apps misbehave, such as things like the [Cineplex app](https://play.google.com/store/apps/details?id=com.fivemobile.cineplex) which
      pushes events into an unexpected calendar
  - pros
    * first class on android
    * they are their own standard eco-system
  - cons
    * does not really support standards so other clients will not work
* on the desktop he is using the following software:
  - email
    * [mutt](http://www.mutt.org/)
    * [not much](https://notmuchmail.org/)
    * [imapsync](http://imapsync.lamiral.info/)
  - contacts
    * [khard](https://github.com/scheibler/khard)
    * [vdirsyncer](https://vdirsyncer.pimutils.org/en/stable/)
    * [goobook](https://todoman.readthedocs.io/en/latest/)
  - calendar
    * [khal](https://github.com/pimutils/khal)
    * vdirsyncer
  - todo (sort of)
    * [todoman](https://todoman.readthedocs.io/en/latest/), not really using
      this anymore
    * may move to [taskwarrior](http://taskwarrior.org/) in the future
    * vdirsyncer
* Chris used to use [offlineimap](http://www.offlineimap.org/) but encountered
  sync issues every few months
* imapsync has been working well for 3 years now. Well enough that Chris has to
  remind himself about how to use it for the presentation
* mail is stored locally in [maildir](https://en.wikipedia.org/wiki/Maildir) to
  it can be accessed by multiple clients (e.g., mutt, [Gnome evolution](https://wiki.gnome.org/Apps/Evolution/))
* appears that [Thunderbird](https://www.mozilla.org/en-US/thunderbird/) might
  be getting maildir support soon too (it is mentioned in the CHANGELOG, but not
  officially announced)
* Chris suggests starting with a simple mutt configuration
  - slowly add useful things as you come across things you need
* he also puts maildir into [git](https://git-scm.com/) and sync it to his
[gitlab](https://about.gitlab.com/) server
* security concerns
  - protection from someone walking about with this laptop, and really nothing else
  - keeping in mind that google has clear copies of his emails
  - servers at home are full-disk encrypted and need a password on reboot
* davmail proxy which will contact to [Microsoft Exchange](https://en.wikipedia.org/wiki/Microsoft_Exchange_Server)
  and provides [IMAP](https://en.wikipedia.org/wiki/Internet_Message_Access_Protocol),
  [SMTP](https://en.wikipedia.org/wiki/Simple_Mail_Transfer_Protocol),
  [CalDav](https://en.wikipedia.org/wiki/CalDAV),
  and [LDAP](https://en.wikipedia.org/wiki/Lightweight_Directory_Access_Protocol)
* evolutions components are getting skinnier
  - hooks into the gnome notifications
  - supports google
  - the calendar looks nice and simple
  - they are still backed by the evolution database
* [NextCloud](https://nextcloud.com/) (formerly OwnCloud) could be the next step for more self-hosting
* talking about gitlab hosting
