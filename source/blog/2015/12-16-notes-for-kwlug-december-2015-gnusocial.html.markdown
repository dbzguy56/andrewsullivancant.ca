---
:title: 'Notes for KWLUG December 2015: GNUSocial'
:tags: kwlug
:date: 2015-12-16
---
[Bob Jonkman](http://sobac.com/bjonkman/) presented on [GNUSocial](https://gnu.io/social/) at the [KWLUG](http://kwlug.org) on [December 7 2015 meeting](http://kwlug.org/node/992). These are some of my notes from the night, mostly on topic:

* during the intro [Jeff](https://twitter.com/jeffvoskamp) mentioned that [Let's Encrypt](https://letsencrypt.org/) has gone into beta and he installed it on some of his own servers
  - we discussed doing an [TLS/SSL](https://en.wikipedia.org/wiki/Transport_Layer_Security) presentation, covering the basics and how to use Let's Encrypt
  - would get interesting to add this to [kwlug.org](http://kwlug.org) and other sites that I maintain (e.g., [andrewsullivancant.ca](http://andrewsullivancant.ca), [kwruby.ca](http://kwruby.ca), and [kwdashboard.ca](http://kwdashboard.ca) in the future)
* GNUSocial is implementing various [OStatus](http://www.w3.org/community/ostatus/wiki/Main_Page) protocols
  - is there a value to making an statically generated [Indieweb](http://indiewebcamp.com/) site generate those protocols?
  - how difficult would this be?
  - how to generate [ActivityStream](https://en.wikipedia.org/wiki/Activity_Streams_(format)), [RSS](https://en.wikipedia.org/wiki/RSS), and [Atom](https://en.wikipedia.org/wiki/Atom_(standard)) for [middleman](https://middlemanapp.com/) sites
* is there a GNUSocial [docker](https://www.docker.com/) image?
* can [WebFinger](https://en.wikipedia.org/wiki/WebFinger) or [OpenId](https://openid.net/) be implemented on a static web site?
* various options for publishing avatar images
  - [Gravatar](https://secure.gravatar.com/) is a proprietary and widely used service run by [Automattic](http://automattic.com/), the [WordPress](https://wordpress.com/) company
  - [LibrAvatar](https://www.libravatar.org/) service and federated protocol for hosting an avatar
* [Evan Podromo](https://e14n.com/evan) the creator of [identi.ca](https://identi.ca/), which became StatusNet, which has now been folded into GNUSocial
  - he also started the [pump.io](http://pump.io/) service
  - what is he working on at the moment? is company [E14N](https://e14n.com/)
    is still operating pump.io
* Bob showed off the poll type post in GNUSocial, had anyone defined a poll
  [microformat](http://microformats.org/) for IndieWeb?
* Bob was asking about whether there was any interested in setting up
  a GnuSocial server for KWLUG
  - what would we do with it, and how many of use would participate?
  - would [CCJClearLine](http://ccjclearline.com/) be willing to host this, in
    addition to the regular website?
  - would one of the one-time cost [CloudAtCost](http://cloudatcost.com/) servers work for this?
* there are other clients which can communicate with GNUSocial 2 were
  mentioned:
  - [Choqok](http://choqok.gnufolks.org/)
  - [mustard](https://play.google.com/store/apps/details?id=org.mustard.android)
