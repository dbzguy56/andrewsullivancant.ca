---
title: 'Simple Ruby Key/Value Stores'
# date: TBD When publishing
tags: ruby key value store benchmark gitdocs
---

As described in [Ruby Version Review 2016](/blog/2016-11-05-ruby-version-review-2016/),
I was upgrading [gitdocs](https://github.com/nesquena/gitdocs) gems and am
choosing not to upgrade the [activerecord](https://rubygems.org/gems/activerecord)
to v5.x in order to preserve support for Ruby v2.0. But is ActiveRecord
and SQLite the best option here?

Some reasonable options for storing a small database in a single file:

* [activerecord](https://rubygems.org/gems/activerecord) with [SQLite](https://sqlite.org/)
* [SQLite](https://sqlite.org/) direct
* [PStore](https://ruby-doc.org/stdlib-2.2.3/libdoc/pstore/rdoc/PStore.html)
* [YAML::Store](https://ruby-doc.org/stdlib-1.9.3/libdoc/yaml/rdoc/YAML/Store.html)
* [YAML::DBM](https://ruby-doc.org/stdlib-2.1.2/libdoc/yaml/rdoc/YAML/DBM.html)
* [SDBM](http://ruby-doc.org/stdlib-2.0.0/libdoc/sdbm/rdoc/SDBM.html)


repository records
id [Integer]
path [String]
polling_interval [Double] defaults to 15.0
notification [Boolean] default to true
remote_name [String] default to 'origin'
branch_name [String] default to 'master'
sync_type ['full','fetch'] default to 'full'

# ActiveRecord with SQLite

```ruby
require 'activerecord'
require 'benchmark/ips'
require 'tempfile'

class Configuration < ActiveRecord::Base
end

class Share < ActiveRecord::Base
end

class Migrate < ActiveRecord::Migration
  def self.up
    create_table :configs do |t|
      t.column :start_web_frontend, :boolean, default: true
      t.column :web_frontend_port,  :integer, default: 8888
      t.column :web_frontend_host,  :string,  default: '127.0.0.1'
    end

    create_table :shares do |t|
      t.column :path,             :string
      t.column :polling_interval, :double,  default: 15
      t.column :notification,     :boolean, default: true
      t.column :remote_name,      :string,  default: 'origin'
      t.column :branch_name,      :string,  default: 'master'
      t.column :sync_type,        :string,  default: 'full'
    end
  end
end

ActiveRecord::Base.establish_connection(
  adapter: 'sqlite3', database: Tempfile.new('activerecord')
)
ActiveRecord::Migrator.run(Migrate)

```

# SQLite direct

```ruby
require 'sqlite3'
require 'benchmark/ips'

db = SQLite3::Database.new(Tempfile.new('sqlite')
```

# PStore

```ruby
require 'pstore'
require 'benchmark/ips'
```

# YAML::Store

```ruby
require 'yaml/store'
require 'benchmark/ips'
```

# YAML::DBM

```ruby
require 'yaml/dbm'
require 'benchmark/ips'
```

# SDBM

```ruby
require 'sdbm'
```


# Other research

* [StackOverflow: Ruby PStore vs Postgres](https://stackoverflow.com/questions/38293832/ruby-pstore-vs-postgres/38304674)
* [moneta](https://github.com/minad/moneta) is a unified interface for
  key/value storage systems
* [Rubys built in databases - meet PStore and YAML::Store](https://www.krautcomputing.com/blog/2015/09/21/rubys-built-in-databases-meet-pstore-and-yamlstore/)
